package com.nimbusds.common.contenttype;


import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;

import junit.framework.TestCase;


public class ContentTypeTest extends TestCase {
	
	
	public void testParameter() {
		
		ContentType.Parameter p = new ContentType.Parameter("charset", "UTF-8");
		assertEquals("charset", p.getName());
		assertEquals("UTF-8", p.getValue());
		assertEquals("charset=UTF-8", p.toString());
	}
	
	
	public void testParameterEquality() {
		
		assertTrue(new ContentType.Parameter("charset", "UTF-8").equals(new ContentType.Parameter("charset", "UTF-8")));
		assertTrue(new ContentType.Parameter("CHARSET", "UTF-8").equals(new ContentType.Parameter("charset", "UTF-8")));
		assertTrue(new ContentType.Parameter("CHARSET", "UTF-8").equals(new ContentType.Parameter("charset", "utf-8")));
		
	}
	
	public void testParameterInequality() {
		
		assertFalse(new ContentType.Parameter("charset", "UTF-8").equals(new ContentType.Parameter("b", "c")));
	}
	
	
	public void testParameterHashCode() {
		
		assertEquals(new ContentType.Parameter("charset", "UTF-8").hashCode(), new ContentType.Parameter("charset", "UTF-8").hashCode());
		assertEquals(new ContentType.Parameter("CHARSET", "UTF-8").hashCode(), new ContentType.Parameter("charset", "UTF-8").hashCode());
		assertEquals(new ContentType.Parameter("CHARSET", "UTF-8").hashCode(), new ContentType.Parameter("charset", "utf-8").hashCode());
	}
	
	
	public void testParameterArgCheck() {
		
		try {
			new ContentType.Parameter("charset", null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The parameter value must be specified", e.getMessage());
		}
		
		try {
			new ContentType.Parameter(null, "UTF-8");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The parameter name must be specified", e.getMessage());
		}
		
		try {
			new ContentType.Parameter("charset", "");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The parameter value must be specified", e.getMessage());
		}
		
		try {
			new ContentType.Parameter("", "UTF-8");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The parameter name must be specified", e.getMessage());
		}
	}
	
	
	public void testContentTypeConstants() {
		
		assertEquals("application/json; charset=UTF-8", ContentType.APPLICATION_JSON.toString());
		assertEquals("application/jwt; charset=UTF-8", ContentType.APPLICATION_JWT.toString());
		assertEquals("application/jose; charset=UTF-8", ContentType.APPLICATION_JOSE.toString());
		assertEquals("application/x-www-form-urlencoded; charset=UTF-8", ContentType.APPLICATION_URLENCODED.toString());
		assertEquals("text/plain; charset=UTF-8", ContentType.TEXT_PLAIN.toString());
		assertEquals("image/apng", ContentType.IMAGE_APNG.toString());
		assertEquals("image/avif", ContentType.IMAGE_AVIF.toString());
		assertEquals("image/gif", ContentType.IMAGE_GIF.toString());
		assertEquals("image/jpeg", ContentType.IMAGE_JPEG.toString());
		assertEquals("image/png", ContentType.IMAGE_PNG.toString());
		assertEquals("image/svg+xml", ContentType.IMAGE_SVG_XML.toString());
		assertEquals("image/webp", ContentType.IMAGE_WEBP.toString());
		assertEquals("application/pdf", ContentType.APPLICATION_PDF.toString());
	}
	
	
	public void testBasic() {
		
		ContentType ct = new ContentType("application", "json");
		assertEquals("application", ct.getBaseType());
		assertEquals("json", ct.getSubType());
		assertEquals("application/json", ct.getType());
		assertTrue(ct.getParameters().isEmpty());
		assertEquals("application/json", ct.toString());
	}
	
	
	public void testWithOneParameter() {
		
		ContentType ct = new ContentType("application", "json", new ContentType.Parameter("charset", "UTF-8"));
		assertEquals("application", ct.getBaseType());
		assertEquals("json", ct.getSubType());
		assertEquals("application/json", ct.getType());
		assertEquals(Collections.singletonList(new ContentType.Parameter("charset", "UTF-8")), ct.getParameters());
		assertEquals("application/json; charset=UTF-8", ct.toString());
	}
	
	
	public void testWithTwoParameters() {
		
		ContentType ct = new ContentType("application", "json", new ContentType.Parameter("charset", "UTF-8"), new ContentType.Parameter("b", "c"));
		assertEquals("application", ct.getBaseType());
		assertEquals("json", ct.getSubType());
		assertEquals("application/json", ct.getType());
		assertEquals(Arrays.asList(new ContentType.Parameter("charset", "UTF-8"), new ContentType.Parameter("b", "c")), ct.getParameters());
		assertEquals("application/json; charset=UTF-8; b=c", ct.toString());
	}
	
	
	public void testCharsetConstructor() {
		
		ContentType ct = new ContentType("application", "json", StandardCharsets.UTF_8);
		assertEquals("application/json; charset=UTF-8", ct.toString());
	}
	
	
	public void testConstructor_nullBaseType() {
		
		try {
			new ContentType(null, "json");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The base type must be specified", e.getMessage());
		}
	}
	
	
	public void testConstructor_nullSubType() {
		
		try {
			new ContentType("application", null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The subtype must be specified", e.getMessage());
		}
	}


	public void testSuffix() {

		ContentType ct = new ContentType("application", "entity-type+jwt");
		assertEquals("application", ct.getBaseType());
		assertEquals("entity-type+jwt", ct.getSubType());
		assertEquals("entity-type", ct.getBaseSubType());
		assertEquals("jwt", ct.getSubTypeSuffix());
		assertTrue(ct.hasSubTypeSuffix("jwt"));
		assertFalse(ct.hasSubTypeSuffix("json"));
		assertFalse(ct.hasSubTypeSuffix(null));
	}


	public void testSuffix_none() {

		ContentType ct = new ContentType("application", "json");
		assertEquals("application", ct.getBaseType());
		assertEquals("json", ct.getSubType());
		assertEquals("json", ct.getBaseSubType());
		assertNull(ct.getSubTypeSuffix());
		assertFalse(ct.hasSubTypeSuffix("json"));
		assertFalse(ct.hasSubTypeSuffix(null));
	}
	
	
	public void testMatchesTrue() {
		
		assertTrue(ContentType.APPLICATION_JSON.matches(new ContentType("application", "json")));
		assertTrue(ContentType.APPLICATION_JSON.matches(new ContentType("APPLICATION", "json")));
		assertTrue(ContentType.APPLICATION_JSON.matches(new ContentType("application", "JSON")));
		assertTrue(ContentType.APPLICATION_JSON.matches(new ContentType("APPLICATION", "JSON")));
		
		assertTrue(ContentType.APPLICATION_JSON.matches(new ContentType("application", "json", new ContentType.Parameter("a", "b"))));
	}

	
	public void testMatchesFalse() {
		
		assertFalse(ContentType.APPLICATION_JSON.matches(ContentType.APPLICATION_JOSE));
		assertFalse(ContentType.APPLICATION_JSON.matches(null));
	}
	
	
	public void testEqualityAndHashCode() {
		
		ContentType ct1 = new ContentType("application", "json");
		ContentType ct2 = new ContentType("APPLICATION", "json");
		ContentType ct3 = new ContentType("application", "JSON");
		ContentType ct4 = new ContentType("APPLICATION", "JSON");
		
		assertTrue(ct1.equals(ct2));
		assertTrue(ct1.equals(ct3));
		assertTrue(ct1.equals(ct4));
		
		assertEquals(ct1.hashCode(), ct2.hashCode());
		assertEquals(ct1.hashCode(), ct3.hashCode());
		assertEquals(ct1.hashCode(), ct4.hashCode());
	}
	
	
	public void testEqualityAndHashCode_withParameter() {
		
		ContentType ct1 = new ContentType("application", "json", StandardCharsets.UTF_8);
		ContentType ct2 = new ContentType("APPLICATION", "json", StandardCharsets.UTF_8);
		ContentType ct3 = new ContentType("application", "JSON", StandardCharsets.UTF_8);
		ContentType ct4 = new ContentType("APPLICATION", "JSON", StandardCharsets.UTF_8);
		
		assertTrue(ct1.equals(ct2));
		assertTrue(ct1.equals(ct3));
		assertTrue(ct1.equals(ct4));
		
		assertEquals(ct1.hashCode(), ct2.hashCode());
		assertEquals(ct1.hashCode(), ct3.hashCode());
		assertEquals(ct1.hashCode(), ct4.hashCode());
	}
	
	
	public void testInequalityAndHashCode() {
		
		ContentType ct1 = new ContentType("application", "json");
		ContentType ct2 = new ContentType("image", "png");
		
		assertFalse(ct1.equals(ct2));
		
		assertNotSame(ct1.hashCode(), ct2.hashCode());
	}
	
	
	public void testInequalityAndHashCode_withParameter() {
		
		ContentType ct1 = new ContentType("application", "json", StandardCharsets.UTF_8);
		ContentType ct2 = new ContentType("image", "png");
		
		assertFalse(ct1.equals(ct2));
		
		assertNotSame(ct1.hashCode(), ct2.hashCode());
	}
	
	
	public void testParseBasic()
		throws ParseException  {
		
		for (String s: new String[]{
			"application/json",
			"application/ json",
			"application /json",
			"application / json",
			" application / json",
			"application/json;",
			"application/json ;",
			"application/json ; "
			
		}) {
			
			ContentType ct = ContentType.parse(s);
			assertEquals("application", ct.getBaseType());
			assertEquals("json", ct.getSubType());
			assertTrue(ct.getParameters().isEmpty());
		}
	}
	
	
	public void testParseWithOneParam()
		throws ParseException  {
		
		for (String s: new String[] {
			"application/json;charset=UTF-8",
			"application/json; charset=UTF-8",
			"application/json ; charset=UTF-8",
			"application/json; charset=UTF-8 ",
			"application/json; charset = UTF-8",
			"application/json ; charset = UTF-8 ",
			" application / json ; charset = UTF-8 ",
			
		}) {
			
			ContentType ct = ContentType.parse(s);
			assertEquals("application", ct.getBaseType());
			assertEquals("json", ct.getSubType());
			assertEquals(Collections.singletonList(new ContentType.Parameter("charset", "UTF-8")), ct.getParameters());
		}
	}
	
	
	public void testParseWithTwoParams()
		throws ParseException  {
		
		for (String s: new String[] {
			"application/json;charset=UTF-8;b=c",
			"application/json; charset=UTF-8; b=c",
			"application/json ; charset=UTF-8 ; b=c",
			"application/json; charset=UTF-8 ; b=c",
			"application/json; charset = UTF-8 ; b = c",
			"application/json ; charset = UTF-8 ; b = c",
			" application / json ; charset = UTF-8 ; b = c",
			
		}) {
			
			ContentType ct = ContentType.parse(s);
			assertEquals("application", ct.getBaseType());
			assertEquals("json", ct.getSubType());
			assertEquals(s, Arrays.asList(new ContentType.Parameter("charset", "UTF-8"), new ContentType.Parameter("b", "c")), ct.getParameters());
		}
	}
	
	
	public void testParse_null() {
		
		try {
			ContentType.parse(null);
			fail();
		} catch (ParseException e) {
			assertEquals("Null or empty content type string", e.getMessage());
		}
	}
	
	
	public void testParse_empty() {
		
		try {
			ContentType.parse("");
			fail();
		} catch (ParseException e) {
			assertEquals("Null or empty content type string", e.getMessage());
		}
	}
	
	
	public void testParse_blank() {
		
		try {
			ContentType.parse(" ");
			fail();
		} catch (ParseException e) {
			assertEquals("Null or empty content type string", e.getMessage());
		}
	}
	
	
	public void testParse_missingDashDelimiter() {
		
		try {
			ContentType.parse("application");
			fail();
		} catch (ParseException e) {
			assertEquals("Invalid content type string", e.getMessage());
		}
	}
	
	
	public void testParse_invalidParameter() {
		
		try {
			ContentType.parse("application/json ; =");
			fail();
		} catch (ParseException e) {
			assertEquals("Invalid parameter", e.getMessage());
		}
	}
	
	
	public void testParse_noParameterName() {
		
		try {
			ContentType.parse("application/json ; =UTF-8");
			fail();
		} catch (ParseException e) {
			assertEquals("Invalid parameter", e.getMessage());
		}
	}
	
	
	public void testParse_noParameterValue() {
		
		try {
			ContentType.parse("application/json ; charset=");
			fail();
		} catch (ParseException e) {
			assertEquals("Invalid parameter", e.getMessage());
		}
	}
}
