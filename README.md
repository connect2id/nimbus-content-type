# Nimbus Content Type

Java 7+ library for representing and parsing Content (Media) Types. No external
runtime dependencies. Apache 2.0 license.


## Maven

```
<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>nimbus-content-type</artifactId>
    <version>[ version ]</version>
</dependency>
```

where `[ version ]` is the latest stable version.


## Usage

To create a new content type `application/json` without character set 
parameter:

```
import com.nimbusds.common.contenttype.ContentType;

ContentType ct = new ContentType("application", "json");

// Prints out "application/json"
System.out.println(ct.toString());
```

With a character set parameter `application/json; charset=UTF-8`:

```
ContentType ct = new ContentType("application", "json", new ContentType.Parameter("charset", "UTF-8"));

// Prints out "application/json; charset=UTF-8"
System.out.println(ct.toString());
```

To parse a content type:

```
try {
        ContentType.parse("application/json; charset=UTF-8");
} catch (java.text.ParseException e) {
        System.err.println(e.getMessage());
}
```


## Bug reports and suggestions

To post bug reports and suggestions:

<https://bitbucket.org/connect2id/nimbus-content-type/issues>


## News

Follow us on Twitter for updates and new releases:

<https://twitter.com/connect2id>


## Change log

* version 1.0 (2020-02-05)
    * First release.

* version 2.0 (2020-02-05)
    * Renames ContentType.getType to getBaseType.
    * Renames ContentType.getSubtype to getSubType.

* version 2.1 (2020-05-28)
    * Adds ContentType.getType method.
    * Adds ContentType.PLAIN_TEXT constant for text/plain; charset=UTF-8

* version 2.2 (2021-11-24)
    * Adds constants for common image types.

* version 2.3 (2023-11-05)
    * Adds suffix support, e.g. "+xml", "+json", etc. 
